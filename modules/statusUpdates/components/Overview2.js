import React, { useEffect, useState } from 'react';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import Card from 'antd/lib/card';
// antd components
import DatePicker from 'antd/lib/date-picker';
import Cookies from 'universal-cookie';
import Chart from 'react-google-charts'

import dataFetch from '../../../utils/dataFetch';
import Statsdash from './Statsdash';
import Doughnut from './Doughnut';

const { RangePicker } = DatePicker;
const moment = extendMoment(Moment);
const cookies = new Cookies();

const Overview2 = () => {
  const usernamecookie=cookies.get('username');
  const [data, setData] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [pieData, setPieData] = useState([]);
  var numberDays=7;

  const [userName,setUserName] = useState([]);
  const [days,setDays] = useState(numberDays);
  const [given,setGiven] = useState(0);
  const [notGiven,setNotGiven] = useState(0);

  const [rangeLoaded, setRangeLoaded] = useState(false);
  const [isLoaded, setLoaded] = useState(false);

  const query = `query ($startDate: Date!, $endDate: Date){
  clubStatusUpdate(startDate: $startDate, endDate: $endDate){
    dailyLog{
      date
      membersSentCount
    }
    memberStats{
      user{
        username
        admissionYear
      }
      statusCount
    }
  }
}`;


  const fetchData = async (variables) => await dataFetch({ query, variables });

  useEffect(() => {
    if (!rangeLoaded) {
      setStartDate(new Date(moment().subtract(1, 'weeks').format('YYYY-MM-DD')));
      setRangeLoaded(true);
      numberDays=7;
    }
    
      // console.log(moment(endDate).diff(moment(startDate), 'days'));
    if (!isLoaded && rangeLoaded) {
      const variables = {
        startDate: moment(startDate).format('YYYY-MM-DD'),
        endDate: moment(endDate).format('YYYY-MM-DD'),
      };
      
      fetchData(variables).then((r) => {
        // console.log(r.data.clubStatusUpdate.memberStats.user.me.username);
        r.data.clubStatusUpdate.memberStats.map((item)=> {
          setUserName(usernamecookie)
          // console.log(userName)
          if(item.user.username === userName) {
            var start = moment(startDate);
            var end = moment(endDate);
            const arr=[['status', 'count'],
            ['Updates given'],
            ['Not given'],];
            numberDays=moment(end).diff(moment(start), 'days');
            setDays(numberDays);
            arr[1].push(parseInt(item.statusCount));
            setGiven(item.statusCount);
            arr[2].push(numberDays-item.statusCount);
            setNotGiven(numberDays-item.statusCount);
            setPieData(arr);
            // console.log(arr);
            // console.log(userName);
          }
        });
        setData(r.data.clubStatusUpdate.memberStats);
        setLoaded(true);
      });
    }
  });

  

  const handleRangeChange = (obj) => {
    if (obj[0] != null && obj[1] != null) {
      setStartDate(moment(obj[0]));
      setEndDate(moment(obj[1]));   
      setLoaded(false);
      numberDays=moment(endDate).diff(moment(startDate), 'days');

    }
  };


  return (
    <div className="p-4">
      <div className="mx-2">
        <div className="row m-0">
          <div className="col text-right">
            <RangePicker
              defaultValue={[
                moment(new Date(), 'YYYY-MM-DD').subtract(1, 'weeks'),
                moment(new Date(), 'YYYY-MM-DD'),
              ]}
              onChange={
                handleRangeChange
              }
            />
          </div>
        </div>
      </div>
      <div className="row m-0">   
        <div className="col-md-6 p-2">
             <Card style={{ boxShadow: '10px 10px 5px rgba(0, 0, 0, 0.25)'}}>
                      <div class="flex-container"
                           style={{ display:'flex', flexBasis: 0 }}>
                          <div class="flex-child yellow"
                             style={{ flex:1, textAlign:'center'}}>
                              <h3>{days}</h3>
                              <h7>Total number of days</h7>
                          </div>
                          <div class="flex-child red"
                             style={{ flex:1, textAlign:'center'}}>
                              <h3>{given}</h3>
                              <h7>Updates given</h7>
                          </div>
                          <div class="flex-child blue"
                             style={{ flex:1, textAlign:'center'}}>
                              <h3>{notGiven}</h3>
                              <h7>Updates missed</h7>
                          </div>
                      </div>
            </Card>
            <Card style={{ boxShadow: '10px 10px 5px rgba(0, 0, 0, 0.25)', marginTop: '20px'}}>
              <div><p>Number of times updated first:👍</p></div>
              <div><p>Number of times updated last:🐌</p></div>
              <div><p>Number of times got kicked:☠</p></div>
            </Card>
        </div>
        <div className="col-md-6 p-2">
             
             {isLoaded? <Card style={{backgroundColor: '#FFFFFF', boxShadow: '10px 10px 5px rgba(0, 0, 0, 0.25)'}}>
                           <Doughnut pdata={pieData} isLoaded={true}/>
                        </Card>: <Card style={{backgroundColor: '#FFFFFF', boxShadow: '10px 10px 5px rgba(0, 0, 0, 0.25)'}}>
                        <img className="col-md-6 p-2" src="/static/images/loading.gif" alt="image"/>
                        </Card>}                  
        </div>
        <div className="col-md-12 p-2">
               <Card style={{backgroundColor: '#FFFFFF', boxShadow: '10px 10px 5px rgba(0, 0, 0, 0.25)'}}>
                  <Statsdash data={data} isLoaded={true} />
                </Card>
        </div>
      </div>
    </div>
  );
};

export default Overview2;
